use async_std::task;
use surf;
use versionstore::WebFile;
use html5ever::tokenizer::{
    BufferQueue, Tag, TagKind, TagToken, Token, TokenSink, TokenSinkResult, Tokenizer,
    TokenizerOpts,
};

use std::borrow::Borrow;
use url::{ParseError, Url};

#[derive(Default, Debug)]
pub struct LinkQueue {
    links: Vec<String>,
}


pub fn in_reference_list(link: &Url) -> bool{
    false
    /*
    let host_str = link.host_str().unwrap();
    let path = link.path();
    let query = link.query().unwrap();

    let needle = format!("{scheme} {host_str} {path} {query}", scheme="https", host_str=host_str, path=path, query=query); 

    if let Some(str) = history.links.iter().find(|&s| *s == needle){
        true
    }else{
        history.links.push(needle);
        false
    } */

} 


impl TokenSink for &mut LinkQueue {
    type Handle = ();

    // Example for parsing: <a href="link">some text</a>
    fn process_token(&mut self, token: Token, _line_number: u64) -> TokenSinkResult<Self::Handle> {
        match token {
            TagToken(
                ref
                tag
                @
                Tag {
                    kind: TagKind::StartTag,
                    ..
                },
            ) => {
                if tag.name.as_ref() == "a" {
                    for attribute in tag.attrs.iter() {
                        if attribute.name.local.as_ref() == "href" {
                            let url_str: &[u8] = attribute.value.borrow();
                            self.links
                                .push(String::from_utf8_lossy(url_str).into_owned());
                        }
                    }
                }
            }

            _ => {}
        }
        TokenSinkResult::Continue
    }
}

pub fn get_links(url: &Url, page: String) -> Vec<Url> {
    let mut domain_url = url.clone();
    domain_url.set_path("");
    domain_url.set_query(None);

    let mut queue = LinkQueue::default();
    let mut tokenizer = Tokenizer::new(&mut queue, TokenizerOpts::default());
    let mut buffer = BufferQueue::new();
    buffer.push_back(page.into());
    let _ = tokenizer.feed(&mut buffer);

    queue
        .links
        .iter()
        .map(|link| match Url::parse(link) {
            Err(ParseError::RelativeUrlWithoutBase) =>{ 
                //println!("without base: {}", link);
                domain_url.join(link).unwrap()},
            Err(_) => {
                let link = link.replace(" ", "");
                domain_url.join(&link).unwrap()
            } //panic!("Malformed link found: {}", link),
            Ok(url) => url,
        })
        //.map(|link| link)
        .collect()
}

type CrawlResult = Result<(), Box<dyn std::error::Error + Send + Sync + 'static>>;
type BoxFuture = std::pin::Pin<Box<dyn std::future::Future<Output = CrawlResult> + Send>>;

fn box_crawl( pages: Vec<Url>, current: u8, max: u8) -> BoxFuture {
    //println!("first {:?}", pages);
    Box::pin(crawl(pages, current, max))
}

async fn crawl( pages: Vec<Url>, current: u8, max: u8) -> CrawlResult {
    if current > max {
        return Ok(());
    }

    let mut tasks = vec![];
    for url in pages {
        let task = task::spawn(async move {
            
            let destination = "D:/repos/repo123/".to_string();
            let mut res = surf::get(&url).await?;
            let body = res.body_string().await?;
            // todo: read more on the async_std on how to use this tasks
            // and how it relates to std input output operations that block.
            // this seems not the correct place, seeing we are in concurrent code
            // from the runtime used by the future lib aync_std.
            let file1 = WebFile::new(url.clone(), body.clone(), destination.clone());
            let _ = file1.save();
            //println!("file: {}", file1.name);
            let links = get_links(&url, body)
                .into_iter()
                .filter(|link| {
                    // Todo: make dynamic domain
                    if link.domain() == Some("www.verf.nl") {
                        true
                    } else {
                        false
                    }
                }).filter(|link|{
                    if in_reference_list(link) {
                        false
                    }else{
                        true
                    }
                })
                .collect();

            //println!("vec links {:?}",links);    
            // recursive
            box_crawl( links, current + 1, max).await
        });
        tasks.push(task);
    }

    for task in tasks.into_iter() {
        task.await?;
    }

    Ok(())
}

pub fn run(_url: String) -> CrawlResult {
    println!("Start Run");

    let p = task::block_on(async {
        // todo make dynamic domain
        box_crawl( vec![Url::parse("https://www.verf.nl").unwrap()], 1, 3).await
    });
    println!("Finished Run");
    p
}
